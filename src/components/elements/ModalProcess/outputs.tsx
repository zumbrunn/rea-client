import React from 'react';
import styled from 'styled-components';
import Commitment from '../../../types/Commitment';
import Popup from './popup';
import moment from 'moment';
import Pencil from '../../../icons/pencil.png';
import { compose, withState } from 'recompose';
import Feed from '../Event';

interface Props {
  activeIntents: Commitment[];
  providerId: string;
  providerImage: string;
  processId: number;
  isFeedOpen: boolean;
  onFeedOpen(boolean): boolean;
  toggleValidationModal(): boolean;
}

const Todo = (props: Props) => {
  return (
    <WrapperIntents>
      {props.activeIntents.map((intent, i) => (
        <Wrapper>
          <Sentence>
            <SentenceContent>
              {`${intent.action} ${intent.committedQuantity.numericValue} ${
                intent.committedQuantity.unit
                  ? intent.committedQuantity.unit.name
                  : ''
              } of ${intent.resourceClassifiedAs.name} in ${
                intent.scope ? intent.scope.name : 'null'
              }`}
            </SentenceContent>
            {props.isFeedOpen ? (
              <>
                <Feeds>
                  {intent.fulfilledBy.map((ev, i) => (
                    <Feed
                      commitmentId={intent.id}
                      scopeId={intent.scope.id}
                      image={ev.fulfilledBy.provider.image}
                      key={i}
                      id={ev.fulfilledBy.id}
                      loggedUserId={props.providerId}
                      providerId={ev.fulfilledBy.provider.id}
                      withValidation
                      withDelete
                      validations={ev.fulfilledBy.validations}
                      primary={
                        <FeedItem>
                          <B>
                            {ev.fulfilledBy.provider
                              ? ev.fulfilledBy.provider.name
                              : ''}
                          </B>{' '}
                          {ev.fulfilledBy.action +
                            ' ' +
                            ev.fulfilledBy.affectedQuantity.numericValue +
                            ' ' +
                            ev.fulfilledBy.affectedQuantity.unit.name +
                            ' of '}
                          <i>
                            {ev.fulfilledBy.affects.resourceClassifiedAs.name}
                          </i>
                        </FeedItem>
                      }
                      secondary={ev.fulfilledBy.note}
                      date={moment(ev.fulfilledBy.start).format('DD MMM')}
                    />
                  ))}
                </Feeds>
                <Popup
                  key={i + 1000}
                  output={intent}
                  onFeedOpen={props.onFeedOpen}
                  processId={props.processId}
                  providerId={props.providerId}
                  providerImage={props.providerImage}
                />
              </>
            ) : (
              <Actions onClick={() => props.onFeedOpen(true)}>
                <ActionSpan>
                  <SpanIcon style={{ backgroundImage: `url(${Pencil})` }} />(
                  {intent.fulfilledBy.length})
                </ActionSpan>
              </Actions>
            )}
          </Sentence>
        </Wrapper>
      ))}
    </WrapperIntents>
  );
};

export default compose(withState('isFeedOpen', 'onFeedOpen', false))(Todo);

const ActionSpan = styled.div`
  font-weight: 500;
  color: #282b30;
  font-size: 13px;
  padding-left: 8px;
  padding-bottom: 8px;
  color: #282b3082;
  margin-top: 10px;
  display: inline-block;
  cursor: pointer;
  z-index: 99999;
`;
const SpanIcon = styled.div`
  cursor: pointer;
  margin-right: 8px;
  display: inline-block;
  width: 18px;
  height: 18px;
  background-size: contain;
  vertical-align: sub;B
`;
const Feeds = styled.div``;

const FeedItem = styled.div`
  font-size: ${props => props.theme.styles.fontSize.sm};
  color: ${props => props.theme.styles.colour.base1};
`;

const B = styled.b`
  font-weight: 500;
  color: #32211b;
`;

const Actions = styled.div`
  padding-bottom: 0px;
  padding-bottom: 0px;

  border-top: 1px solid #e5e5e5;
  cursor: pointer;
  &:hover {
    background: #ececec50;
  }
  margin-top: 8px;
`;

const SentenceContent = styled.div`
  padding: 8px;
  font-weight: 500;
  font-size: 15px;
  color: #16394d;
`;
const WrapperIntents = styled.div`
  position: relative;
`;

const Wrapper = styled.div`
  position: relative;
  margin-bottom: 8px;
  cursor: pointer;
`;

const Sentence = styled.div`
  padding-bottom: 0px;
  margin-top: 8px;
  border-radius: 6px;
  background: #fff;
  border: 5px solid #d2d5da;
`;
