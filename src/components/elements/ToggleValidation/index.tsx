import React from "react";
import styled from "styled-components";
import  {Trash}  from "../Icons";
import Button from "../Button/Button";
import { compose } from "recompose";
const CREATE_VALIDATION = require("../../../graphql/mutations/createValidation");
const DELETE_VALIDATION = require("../../../graphql/mutations/deleteValidation");
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import withNotif from "../../NotificationFactory";

interface Props {
    eventId: string;
    providerId?: string;
    note: string,
    onError: any
    onSuccess: any
    validationId?: string
}

const ValidationFragment = gql`
  fragment myEvent on EconomicEvent {
    validations {
      id
      note
      validatedBy {
        name
        id
      }
    }
  }
`;

const AddValidation:React.SFC<Props> = ({ eventId, providerId, note, onError, onSuccess }) => {
  return (
    <Mutation
      mutation={CREATE_VALIDATION}
      onError={onError}
      update={(store, { data: { createValidation } }) => {
        const event:any = store.readFragment({
          id: `EconomicEvent-${eventId}`,
          fragment: ValidationFragment
        });

        event.validations.push({
          id: createValidation.validation.id,
          note: createValidation.validation.note,
          validatedBy: {
            id: createValidation.validation.validatedBy.id,
            name: createValidation.validation.validatedBy.name,
            __typename: "Person"
          },
          __typename: "Validation"
        });

        store.writeFragment({
          id: `EconomicEvent-${eventId}`,
          fragment: ValidationFragment,
          data: event
        });
        return onSuccess();
      }}
    >
      {(createValidation, { data }) => (
        <Button
          style={{ float: "right", borderRadius: 0 }}
          onClick={() =>
            createValidation({
              variables: {
                token: localStorage.getItem("oce_token"),
                validatedById: providerId,
                economicEventId: eventId,
                note: note
              }
            })
          }
        >
          Validate
        </Button>
      )}
    </Mutation>
  );
};

export default compose(
    withNotif("Validation correctly updated", "Validation is not updated")
  )(AddValidation)

const DeleteValidationComp: React.SFC<Props> = ({ eventId, validationId, onError, onSuccess }) => (
  <Mutation
    mutation={DELETE_VALIDATION}
    onError={onError}
    update={(store, { data: { createValidation } }) => {
      const event:any = store.readFragment({
        id: `EconomicEvent-${eventId}`,
        fragment: ValidationFragment
      });

      let ValIndex = event.validations.findIndex(
        item => Number(item.id) === Number(validationId)
      );
      event.validations.splice(ValIndex, 1);
      store.writeFragment({
        id: `EconomicEvent-${eventId}`,
        fragment: ValidationFragment,
        data: event
      });
      return onSuccess();
    }}
  >
    {(deleteValidation, { data }) => (
      <Button
        primary
        onClick={() =>
          deleteValidation({
            variables: {
              token: localStorage.getItem("oce_token"),
              id: validationId
            }
          })
        }
      >
        <Span><Trash width={18} height={18} strokeWidth={1} color="#f0f0f0" /></Span>
        Delete validation
      </Button>
    )}
  </Mutation>
);

export const DeleteValidation = compose(
    withNotif("Validation correctly updated", "Validation is not updated")
  )(DeleteValidationComp)

const Span = styled.span`
vertical-align: sub;
margin-right: 8px;
`
