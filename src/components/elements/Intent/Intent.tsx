import React from 'react';
import styled, { css } from '../../../themes/styled';
import { clearFix } from 'polished';
import moment from 'moment';
import Feed from '../Event';
import LogEvent from '../../elements/LogEvent';
import CommitmentStatus from './CommitmentStatus';
import { AddProviderComponent, DeleteProvider } from './AddProvider';
import { compose, withState, withHandlers } from 'recompose';
import EditNote from './EditNote';
import EditSentence from './editSentence';
import { ApolloConsumer } from 'react-apollo';
import ButtonDeleteIntent from './DeleteIntent';
import EditDueDate from './EditDueDate';
import Pencil from '../../../icons/pencil.png';
import Commitment from '../../../types/Commitment';

interface Props {
  data: Commitment;
  handleNoteOpen(): boolean;
  isNoteOpen: boolean;
  isSentenceOpen: boolean;
  handleSentenceOpen(): boolean;
  handleFeedOpen(): boolean;
  isFeedOpen: boolean;
  toggleValidationModal(): void;
  providerId: string;
  scopeId: string;
}

const IntentComponent: React.SFC<Props> = props => {
  const {
    data,
    handleNoteOpen,
    isNoteOpen,
    isSentenceOpen,
    handleSentenceOpen,
    scopeId,
    handleFeedOpen,
    isFeedOpen,
    toggleValidationModal,
    providerId
  } = props;
  return (
    <Intent isFinished={data.isFinished}>
      <Wrapper>
        <First>
          {isSentenceOpen ? (
            <ApolloConsumer>
              {client => (
                <EditSentence
                  client={client}
                  handleSentenceOpen={handleSentenceOpen}
                  intent={data}
                />
              )}
            </ApolloConsumer>
          ) : (
            <Sentence isFinished={data.isFinished}>
              <EditDueDate due={data.due} intentId={data.id} />
              <CommitmentStatus
                intentId={data.id}
                isFinished={data.isFinished}
              />
              {data.provider ? (
                <Img
                  style={{ backgroundImage: `url(${data.provider.image})` }}
                />
              ) : null}
              {!data.provider ? (
                <AddProviderComponent
                  providerId={providerId}
                  intentId={data.id}
                />
              ) : data.provider && data.provider.id === providerId ? (
                <DeleteProvider intentId={data.id} />
              ) : null}
              <Text
                onClick={
                  data.userIsAuthorizedToUpdate
                    ? handleSentenceOpen
                    : () => false
                }
              >{`${data.action} ${data.committedQuantity.numericValue} ${
                data.committedQuantity.unit
                  ? data.committedQuantity.unit.name
                  : ''
              } of ${data.resourceClassifiedAs.name} in ${
                data.scope ? data.scope.name : 'null'
              }`}</Text>
            </Sentence>
          )}
          <Note update={data.userIsAuthorizedToUpdate ? true : false}>
            {isNoteOpen && data.userIsAuthorizedToUpdate ? (
              <EditNote intent={data} handleNoteOpen={handleNoteOpen} />
            ) : (
              <span
                style={{ display: 'block' }}
                onClick={
                  data.userIsAuthorizedToUpdate ? handleNoteOpen : () => null
                }
              >
                {data.note || data.userIsAuthorizedToUpdate
                  ? data.note
                  : 'Edit the note'}
              </span>
            )}
          </Note>
        </First>
        <Second>
          <FirstInfo>
            <Agents>
              <span style={{ verticalAlign: 'middle', position: 'relative' }}>
                {data.userIsAuthorizedToDelete ? (
                  <ButtonDeleteIntent intentId={data.id} scopeId={scopeId} />
                ) : null}
              </span>
            </Agents>
          </FirstInfo>
        </Second>
      </Wrapper>
      {isFeedOpen ? (
        <Events>
          {data.fulfilledBy.map((ev, i) => (
            <Feed
              commitmentId={data.id}
              scopeId={scopeId}
              image={ev.fulfilledBy.provider.image}
              key={i}
              id={ev.fulfilledBy.id}
              loggedUserId={providerId}
              providerId={ev.fulfilledBy.provider.id}
              withValidation
              withDelete
              validations={ev.fulfilledBy.validations}
              openValidationModal={toggleValidationModal}
              primary={
                <FeedItem>
                  <B>
                    {ev.fulfilledBy.provider
                      ? ev.fulfilledBy.provider.name
                      : ''}
                  </B>{' '}
                  {ev.fulfilledBy.action +
                    ' ' +
                    ev.fulfilledBy.affectedQuantity.numericValue +
                    ' ' +
                    ev.fulfilledBy.affectedQuantity.unit.name +
                    ' of '}
                  <i>{ev.fulfilledBy.affects.resourceClassifiedAs.name}</i>
                </FeedItem>
              }
              secondary={ev.fulfilledBy.note}
              date={moment(ev.fulfilledBy.start).format('DD MMM')}
            />
          ))}
          <WrapperLogEvent>
            <LogEvent
              commitmentId={data.id}
              providerId={providerId}
              scopeId={scopeId}
              action={data.action}
              unit={data.committedQuantity.unit.name}
              unitId={data.committedQuantity.unit.id}
              resourceId={data.resourceClassifiedAs.id}
              resource={data.resourceClassifiedAs.name}
              closeLogEvent={handleFeedOpen}
            />
          </WrapperLogEvent>
        </Events>
      ) : (
        <Actions onClick={handleFeedOpen}>
          <ActionSpan>
            <SpanIcon style={{ backgroundImage: `url(${Pencil})` }} />(
            {data.fulfilledBy.length})
          </ActionSpan>
        </Actions>
      )}
    </Intent>
  );
};

export default compose(
  withState('isNoteOpen', 'onNoteOpen', false),
  withState('isFeedOpen', 'onFeedOpen', false),
  withState('isSentenceOpen', 'onSentenceOpen', false),
  withHandlers({
    handleNoteOpen: props => () => {
      props.onSentenceOpen(false);
      return props.onNoteOpen(!props.isNoteOpen);
    },
    handleSentenceOpen: props => () => {
      props.onNoteOpen(false);
      return props.onSentenceOpen(!props.isSentenceOpen);
    },
    handleFeedOpen: props => () => {
      props.onNoteOpen(false);
      props.onSentenceOpen(false);
      return props.onFeedOpen(!props.isFeedOpen);
    }
  })
)(IntentComponent);

const Text = styled.span`
  padding: 6px 10px;
  border-radius: 50px;
  cursor: pointer;
  &:hover {
    background: #f8f8f8;
  }
`;

const B = styled.b`
  font-weight: 500;
  color: #32211b;
`;

const ActionSpan = styled.div`
  font-weight: 500;
  color: #282b30;
  font-size: 13px;
  padding-left: 8px;
  padding-bottom: 8px;
  color: #282b3082;
  margin-top: 10px;
  display: inline-block;
  cursor: pointer;
  z-index: 99999;
`;
const SpanIcon = styled.div`
  cursor: pointer;
  margin-right: 8px;
  display: inline-block;
  width: 18px;
  height: 18px;
  background-size: contain;
  vertical-align: sub;B
`;

const WrapperLogEvent = styled.div`
  padding: 10px 0;
  margin: 0 -8px;
  border-top: 1px solid #e9e9e9;
`;

const FeedItem = styled.div`
  font-size: ${props => props.theme.styles.fontSize.sm};
  color: ${props => props.theme.styles.colour.base1};
`;

const Actions = styled.div`
  padding-bottom: 0px;
  padding-bottom: 0px;

  border-top: 1px solid #e5e5e5;
  cursor: pointer;
  &:hover {
    background: #ececec50;
  }
  margin-top: 8px;
`;

const Intent = styled.div<{ isFinished?: boolean }>`
  ${clearFix()};
  margin-top: 0;
  padding-bottom: 0px
  margin-top: 8px;
  border-radius: 6px;
  background: #fff;
  border: ${props =>
    props.isFinished ? '5px solid #62cfa9' : '5px solid #d2d5da'};
`;
const Events = styled.div`
  ${clearFix()};
  position: relative;
  padding: 8px;
`;
const Agents = styled.div`
  ${clearFix()};
  float: right;
  margin-left: 8px;
  vertical-align: sub;
  margin-top: 3px;
`;
const Img = styled.div`
  width: 24px;
  height: 24px;
  background: ${props => props.theme.styles.colour.base2};
  border-radius: 100px;
  display: inline-block;
  margin-right: 8px;
  vertical-align: middle;
  background-size: cover;
`;

const Wrapper = styled.div`
  padding: 8px;
  position: relative;
`;

const First = styled.div`
  ${clearFix()};
`;

const Second = styled.div`
  ${clearFix()};
  position: absolute;
  right: 8px;
  top: 6px;
`;

const FirstInfo = styled.div`
  ${clearFix()};
`;

const Sentence = styled.h3<{ isFinished?: boolean }>`
  font-weight: 500;
  line-height: 24px;
  letter-spacing: 0.5px;
  font-size: 14px;
  text-transform: capitalize;
  color: #32211b;
  margin: 0px !important;

  ${props =>
    props.isFinished &&
    css`
      text-decoration: line-through;
      color: #888;
    `};

  & input {
    vertical-align: text-bottom;
    margin-right: 8px;
    margin-top: 0px;
  }
`;

const Note = styled.h3<{ update?: boolean }>`
  font-weight: 400;
  line-height: 18px;
  font-size: 13px;
  letter-spacing: 0.4px;
  color: ${props => props.theme.styles.colour.base2};
  padding-left: 8px;
  border-left: 1px solid;
  margin: 0;
  margin-top: 8px;
  cursor: ${props => (props.update ? 'pointer' : 'inehrit')};
  &:hover {
    text-decoration: ${props => (props.update ? 'underline' : 'none')};
  }
`;
