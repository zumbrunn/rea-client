import * as React from "react";
const DELETE_COMMITMENT = require("../../../graphql/mutations/deleteCommitment.graphql");
const getCommitments = require('../../../graphql/queries/getCommitments.graphql')
import { Mutation } from "react-apollo";
import {Trash} from '../Icons'
import styled from "../../../themes/styled";
import { compose } from "recompose";
import withNotif from "../../NotificationFactory";

interface Props {
    intentId: string;
    // toggleModal(): boolean;
    scopeId?: string;
    onError: any
    onSuccess: any
}

const DeleteIntent : React.SFC<Props> = ({ onError, onSuccess, intentId, scopeId }) => (
  <Mutation
    mutation={DELETE_COMMITMENT}
    onError={onError}
    update={(store, { data }) => {
        console.log(scopeId)
        let comm: any = store.readQuery({
            query: getCommitments,
            variables: {
              token: localStorage.getItem("oce_token"),
              id: scopeId
            }
          });
          let i = comm.viewer.agent.agentCommitments.findIndex(comm => comm.id === intentId)
          let newlist = comm.viewer.agent.agentCommitments.splice(i, 1)
          store.writeQuery({
            query: getCommitments,
            data: newlist,
            variables: {
              token: localStorage.getItem("oce_token"),
              id: scopeId
            }
          });
      return onSuccess();
    }}
  >
    {(deleteCommitment, { data }) => (
      <Span
        onClick={() =>
          deleteCommitment({
            variables: {
              token: localStorage.getItem("oce_token"),
              id: intentId,
            }
          })
        }
      >
        <Trash width={20} height={20} color={'#dadada'} strokeWidth={2} />
      </Span>
    )}
  </Mutation>
);

const Span = styled.span`
  display: inline-block;
  vertical-align: sub;
  cursor: pointer;
  &:hover {
    svg {
      color: red;
      stroke-width: 2px;
    }
  }
`;

export default compose(
  withNotif(
    "Requirement is successfully deleted",
    "Error! Requirement has not been deleted"
  )
)(DeleteIntent)