import * as React from 'react';
import styled from '../../../themes/styled';
import { withFormik, FormikProps, Field } from 'formik';
import * as Yup from 'yup';
import Events from '../../../util/events';
import Units from '../../../util/units';
import Select from 'react-select';
import Button, { LoaderButton } from '../../elements/Button/Button';
import AsyncSelect from 'react-select/lib/Async';
import { Mutation } from 'react-apollo';
import Commitment from '../../../types/Commitment';
const UPDATE_COMMITMENT = require('../../../graphql/mutations/updateCommitment.graphql');
const GET_ALL_RESOURCES = require('../../../graphql/queries/getAllResources.graphql');
import withNotif from '../../NotificationFactory';
import { compose } from 'recompose';

// import gql from "graphql-tag";

interface Props {
  intent: Commitment;
  handleSentenceOpen(): boolean;
  client: any;
  onError: any;
  onSuccess: any;
}

interface FormValues {
  action: { value: string; label: string };
  numericValue: string;
  unit: {
    value: string;
    label: string;
  };
  affectedResourceClassifiedAsId: {
    value: string;
    label: string;
  };
}

interface MyFormProps {
  intent: Commitment;
  client: any;
  handleSentenceOpen(): boolean;
}

const EditSentenceComponent = (props: Props & FormikProps<FormValues>) => {
  const {
    intent,
    values,
    setFieldValue,
    handleSentenceOpen,
    client,
    onError,
    onSuccess
  } = props;
  const promiseOptions = (client, val) => {
    return client
      .query({
        query: GET_ALL_RESOURCES,
        variables: { token: localStorage.getItem('oce_token') }
      })
      .then(res => {
        let options = res.data.viewer.allResourceClassifications.map(
          resource => ({
            value: resource.id,
            label: resource.name
          })
        );
        if (val === undefined) {
          return options;
        } else {
          let newOpt = options.filter(i => i.label.toLowerCase().includes(val));
          return newOpt;
        }
      })
      .catch(err => console.log(err));
  };
  return (
    <Mutation
      mutation={UPDATE_COMMITMENT}
      onError={onError}
      update={(store, { data: { updateCommitment } }) => {
        // const commitment: any = store.readFragment({
        //   id: `${updateCommitment.commitment.__typename}-${
        //     updateCommitment.commitment.id
        //   }`,
        //   fragment: gql`
        //     fragment myCommitment on Commitment {
        //       id
        //       action
        //       committedQuantity {
        //         numericValue
        //         unit {
        //           id
        //           name
        //         }
        //       }
        //       resourceClassifiedAs {
        //         name
        //         id
        //       }
        //     }
        //   `
        // });
        // console.log(commitment)
        // commitment.action = updateCommitment.commitment.action;
        // commitment.committedQuantity =
        //   updateCommitment.commitment.committedQuantity;
        // commitment.resourceClassifiedAs =
        //   updateCommitment.commitment.resourceClassifiedAs;
        // store.writeFragment({
        //   id: `${updateCommitment.commitment.__typename}-${
        //     updateCommitment.commitment.id
        //   }`,
        //   fragment: gql`
        //     fragment myCommitment on Commitment {
        //       id
        //       action
        //       committedQuantity {
        //         numericValue
        //         unit {
        //           id
        //           name
        //         }
        //       }
        //       resourceClassifiedAs {
        //         name
        //         id
        //       }
        //     }
        //   `,
        //   data: commitment
        // });
        handleSentenceOpen();
        return onSuccess();
      }}
    >
      {(editSentence, { data }) => (
        <WrapperEdit>
          <EditSentence>
            <Field
              name={'action'}
              render={({ field }) => {
                return (
                  <Select
                    placeholder="Select an action..."
                    options={Events}
                    value={field.value}
                    onChange={(val: any) =>
                      setFieldValue('action', {
                        value: val.value,
                        label: val.label
                      })
                    }
                  />
                );
              }}
            />
            <Field
              name="numericValue"
              render={({ field }) => (
                <Input
                  name={field.name}
                  onChange={field.onChange}
                  type="number"
                  min="00.00"
                  max="100.00"
                  step="0.1"
                  placeholder="00.00"
                  value={field.value}
                />
              )}
            />
            <Field
              name={'unit'}
              render={({ field }) => {
                return (
                  <Select
                    placeholder="Select a unit..."
                    options={Units}
                    value={field.value}
                    onChange={(val: any) =>
                      setFieldValue('unit', {
                        value: val.value,
                        label: val.label
                      })
                    }
                  />
                );
              }}
            />
            <Field
              name="affectedResourceClassifiedAsId"
              render={({ field }) => (
                <AsyncSelect
                  placeholder="Select a classification..."
                  defaultOptions
                  cacheOptions
                  value={field.value}
                  styles={customStyles}
                  onChange={(val: any) =>
                    setFieldValue('affectedResourceClassifiedAsId', {
                      value: val.value,
                      label: val.label
                    })
                  }
                  loadOptions={(val: any) => {
                    console.log(val);
                    return promiseOptions(client, val);
                  }}
                />
              )}
            />
          </EditSentence>
          <EditButtons>
            <Button hovered onClick={handleSentenceOpen}>
              Cancel
            </Button>
            <LoaderButton
              loading={props.isSubmitting}
              disabled={props.isSubmitting}
              text={'Edit Sentence'}
              onClick={() =>
                editSentence({
                  variables: {
                    token: localStorage.getItem('oce_token'),
                    id: Number(intent.id),
                    action: values.action.value.toLowerCase(),
                    committedResourceClassifiedAsId: Number(
                      values.affectedResourceClassifiedAsId.value
                    ),
                    committedUnitId: Number(values.unit.value),
                    committedNumericValue: String(values.numericValue)
                  }
                })
              }
            />
          </EditButtons>
        </WrapperEdit>
      )}
    </Mutation>
  );
};

const EditSentenceWithFormik = withFormik<MyFormProps, FormValues>({
  mapPropsToValues: props => ({
    action: { value: props.intent.action, label: props.intent.action },
    numericValue: props.intent.committedQuantity.numericValue,
    unit: {
      value: props.intent.committedQuantity.unit.id,
      label: props.intent.committedQuantity.unit.name
    },
    affectedResourceClassifiedAsId: {
      value: props.intent.resourceClassifiedAs.id,
      label: props.intent.resourceClassifiedAs.name
    }
  }),
  validationSchema: Yup.object().shape({
    action: Yup.object().required(),
    note: Yup.string(),
    numericValue: Yup.number(),
    unit: Yup.object().required(),
    date: Yup.string(),
    affectedResourceClassifiedAsId: Yup.object().required(
      'Classification is a required field'
    )
  }),
  handleSubmit: (values, { props }) => {
    console.log(values);
  }
})(EditSentenceComponent);

export default compose(
  withNotif('Sentence correctly updated', 'Sentence not updated correctly')
)(EditSentenceWithFormik);

const customStyles = {
  control: base => ({
    ...base,
    color: '#333'
  }),
  input: base => ({
    ...base,
    color: '#333'
  }),
  singleValue: base => ({
    ...base,
    color: '#333'
  }),
  placeholder: base => ({
    ...base,
    color: '#333',
    fontSize: '14px'
  })
};

const EditSentence = styled.div`
  z-index: 999999999999;
  position: relative;
  display: grid;
  grid-template-columns: 2fr 1fr 2fr 3fr;
  grid-column-gap: 4px;
`;

const EditButtons = styled.div`
  margin-top: 8px;
  & button {
    margin-right: 8px;
  }
`;

const Input = styled.input`
  border: 1px solid #dadada;
  border-radius: 4px;
  font-size: 14px;
  box-shadow: none;
  text-align: center;
`;

const WrapperEdit = styled.div`
  margin-bottom: 8px;
`;
