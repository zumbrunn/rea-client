import * as React from 'react';
import styled from '../../../themes/styled';
import Process from '../../../types/Process';
import { clearFix } from 'polished';
import moment from 'moment';
import { Clock } from '../Icons';
interface Props {
  process: Process;
  planId: string;
  onOpen(boolean): boolean;
  openModal(string): any;
}

const ProcessComponent: React.SFC<Props> = props => {
  return (
    <Wrapper
      onClick={() =>
        props.openModal(`/plan/${props.planId}/process/${props.process.id}`)
      }
    >
      <Header>
        <Due>
          <span>
            <Clock width={16} height={16} strokeWidth={1} color={'#d4d4d4'} />
          </span>
          {moment(props.process.plannedFinish).format('DD MMM')}
        </Due>
        <Right>
          <Members>
            {props.process.workingAgents
              ? props.process.workingAgents.slice(0, 3).map((a, i) => {
                  return (
                    <Img
                      key={i}
                      style={{
                        backgroundImage: `url(${a.image ||
                          `https://www.gravatar.com/avatar/${
                            a.id
                          }?f=y&d=identicon`})`
                      }}
                    />
                  );
                })
              : null}{' '}
            <Tot>
              {props.process.workingAgents
                ? props.process.workingAgents.length - 3 > 0
                  ? `+ ${props.process.workingAgents.length - 3} More`
                  : ''
                : ''}
            </Tot>
          </Members>
        </Right>
      </Header>
      <Status isFinished={props.process.isFinished}>
        {props.process.isFinished ? 'Completed' : 'Incompleted'}
      </Status>
      <Title>{props.process.name}</Title>
      <Note>{props.process.note}</Note>
      <Actions>
        <Item>{props.process.committedInputs.length} Inputs</Item>
        <Item>{props.process.committedOutputs.length} Outputs</Item>
      </Actions>
    </Wrapper>
  );
};

const Status = styled.div<{ isFinished?: boolean }>`
  background: ${props => (props.isFinished ? 'green' : '#ffcb7d')};
  height: 20px;
  font-size: 12px;
  letter-spacing: 1px;
  line-height: 20px;
  display: inline-block;
  font-weight: 600;
  padding: 0 8px;
  border-radius: 3px;
  color: #fff;
`;

const Item = styled.div`
  letter-spacing: 1px;
  height: 40px;
  line-height: 40px;
  text-align: center;
  font-size: 11px;
  text-transform: uppercase;
  border-radius: 4px;
  background: #f2f4f5;
  font-weight: 500;
  color: #666b75;
`;

const Header = styled.div`
  ${clearFix()};
  padding-bottom: 8px;
  margin: 0 -8px;
  padding: 0 8px;
`;

const Wrapper = styled.div`
  padding: 8px;
  padding-bottom: 0;
  min-height: 200px;
  position: relative;
  cursor: pointer;
  padding-top: 0;
  box-shadow: 0 1px 1px 1px rgba(0, 0, 0, 0.06);
  background: #fff;
  border-radius: 6px;
  &:hover {
    background: #fdfdfd;
  }
`;

const Due = styled.div`
  font-size: 13px;
  color: #d4d4d4;
  font-weight: 600;
  height: 40px;
  line-height: 40px;
  display: inline-block;
  & span {
    vertical-align: sub;
    margin-right: 8px;
  }
`;

const Title = styled.div`
  font-size: 15px;
  margin-top: 4px;
  font-weight: 500;
  color: #44526df7;
  letter-spacing: 0.5px;
`;

const Note = styled.div`
  font-size: 13px;
  margin-top: 4px;
  color: #484b52;
`;

const Tot = styled.div`
  display: inline-block;
  height: 24px;
  line-height: 24px;
  vertical-align: top;
  margin-left: 4px;
  font-size: 13px;
  color: #cacaca;
  font-weight: 600;
`;
const Actions = styled.div`
  padding: 0 8px;
  margin-top: 8px;
  height: 40px;
  position: absolute;
  bottom: 10px;
  left: 0;
  right: 0;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 8px;
  ${clearFix()};
`;
const Right = styled.div`
  float: right;
  height: 40px;
`;
const Members = styled.div`
  height: 40px;
  margin-top: 8px;
  font-size: 12px;
`;

const Img = styled.div`
  width: 24px;
  height: 24px;
  border-radius: 50px;
  display: inline-block;
  margin-left: -4px;
  background-size: cover;
  border: 2px solid white;
`;

export default ProcessComponent;
