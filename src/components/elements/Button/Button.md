### Primary button

```js
<Button style={{marginRight: '10px'}}>Default</Button>

<Button hovered style={{marginRight: '10px'}}>Hover</Button>

<Button active style={{marginRight: '10px'}}>Pressed</Button>

<Button focused style={{marginRight: '10px'}}>Focus</Button>

<Button disabled style={{marginRight: '10px'}}>Disabled</Button>
```

### Secondary button

```js
<Button secondary style={{marginRight: '10px'}}>Default</Button>

<Button secondary hovered style={{marginRight: '10px'}}>Hover</Button>

<Button secondary active style={{marginRight: '10px'}}>Pressed</Button>

<Button secondary focused style={{marginRight: '10px'}}>Focus</Button>

<Button secondary disabled style={{marginRight: '10px'}}>Disabled</Button>
```
