import * as React from 'react';
import { Calendar } from '../Icons';
import Button, { LoaderButton } from '../Button/Button';
import Input from '../../inputs/Text/Text';
import Textarea from '../../inputs/TextArea/Textarea';
import { Form, Field, FormikProps } from 'formik';
import { ApolloConsumer } from 'react-apollo';
import { clearFix, placeholder } from 'polished';
import Select from 'react-select';
import styled from 'styled-components';
import Units from '../../../util/units';
import Events from '../../../util/events';
import AsyncSelect from 'react-select/lib/Async';
import Alert from '../Alert';
import { getAllResources } from '../../../helpers/asyncQueries';
import { FormValues } from './index';
import { formatDate, parseDate } from 'react-day-picker/moment';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

require('react-datepicker/dist/react-datepicker-cssmodules.css');
interface Values {
  value: string;
  label: string;
}

interface Props {
  closeLogEvent(): boolean;
  scopeId: string;
  values: any;
  onError: any;
  onSuccess: any;
}

const LogEvent = (props: Props & FormikProps<FormValues>) => {
  const {
    values,
    setFieldValue,
    errors,
    touched,
    closeLogEvent,
    setFieldTouched,
    scopeId
  } = props;

  return (
    <ApolloConsumer>
      {client => (
        <Form>
          <Module>
            <Log>
              <Row>
                <Action>
                  <Field
                    name={'action'}
                    render={({ field }) => {
                      return (
                        <Select
                          onChange={(val: Values) =>
                            setFieldValue('action', {
                              value: val.value,
                              label: val.label
                            })
                          }
                          options={Events}
                          // styles={customStyles}
                          value={field.value}
                          placeholder="Select an event"
                        />
                      );
                    }}
                  />
                  {errors.action &&
                    touched.action && <Alert>{errors.action}</Alert>}
                </Action>
                <Qty>
                  <Field
                    name="numericValue"
                    render={({ field }) => (
                      <Input
                        name={field.name}
                        onChange={field.onChange}
                        type="number"
                        min="00.00"
                        max="100.00"
                        step="0.1"
                        value={field.value}
                      />
                    )}
                  />
                  {errors.numericValue &&
                    touched.numericValue && (
                      <Alert>{errors.numericValue}</Alert>
                    )}
                </Qty>
                <Unit>
                  <Field
                    name={'unit'}
                    render={({ field }) => {
                      return (
                        <Select
                          onChange={(val: Values) =>
                            setFieldValue('unit', {
                              value: val.value,
                              label: val.label
                            })
                          }
                          options={Units}
                          placeholder="Select a unit"
                          value={field.value}
                        />
                      );
                    }}
                  />
                  {errors.unit && touched.unit && <Alert>{errors.unit}</Alert>}
                </Unit>
                <Resource>
                  <Field
                    name="affectedResourceClassifiedAsId"
                    render={({ field }) => (
                      <AsyncSelect
                        placeholder="Select a classification..."
                        defaultOptions
                        cacheOptions
                        value={field.value}
                        onChange={val =>
                          setFieldValue('affectedResourceClassifiedAsId', {
                            value: val.value,
                            label: val.label
                          })
                        }
                        loadOptions={val =>
                          getAllResources(client, scopeId, val)
                        }
                      />
                    )}
                  />
                  {errors.affectedResourceClassifiedAsId &&
                    touched.affectedResourceClassifiedAsId && (
                      <Alert>{errors.affectedResourceClassifiedAsId}</Alert>
                    )}
                </Resource>
              </Row>
            </Log>
            {/* </div> */}
            <Note>
              <Field
                name="note"
                render={({ field }) => (
                  <Textarea
                    value={field.value}
                    name={field.name}
                    onChange={field.onChange}
                    placeholder={'Type a note...'}
                  />
                )}
              />
            </Note>

            <PublishActions>
              <StartDate
                value={values.date}
                onChange={setFieldValue}
                onBlur={setFieldTouched}
                error={errors.date}
                touched={touched.date}
              />
              <LoaderButton
                loading={props.isSubmitting}
                disabled={props.isSubmitting}
                text={'Publish'}
              />
              {closeLogEvent ? (
                <Button hovered onClick={closeLogEvent}>
                  Cancel
                </Button>
              ) : null}
            </PublishActions>
          </Module>
        </Form>
      )}
    </ApolloConsumer>
  );
};

const Action = styled.div``;
const Qty = styled.div`
  border-radius: 3px;
  max-height: 36px;
  text-align: center;
  & input {
    width: 100% !important;
    max-height: 38px !important;
    line-height: 20px !important;
    min-height: 38px !important;
    padding: 0;
    margin: inherit;
    height: 38px !important;
    border: 1px solid #dadada !important;
    text-align: center;
    color: #333;
    background-color: white !important;
    ${placeholder({ color: '#333' })};
  }
`;
const Unit = styled.div``;
const Resource = styled.div`
  margin-bottom: 8px;
`;
const Row = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr 2fr 3fr;
  grid-column-gap: 4px;
  & input {
    ${placeholder({ color: '#333 !important' })};
  }
`;

const Module = styled.div`
  ${clearFix()};
  position: relative;
  border-radius: 4px;
  z-index: 1;
`;

const Log = styled.div`
  margin-top: 0px;
  padding: 0 10px;
  & input {
    width: 70px;
    float: left;
    background-color: hsl(0, 0%, 98%);
    ${placeholder({ color: '#333 !important' })};
    font-size: 18px;
  }
  ${clearFix()};
`;

const Note = styled.div`
  position: relative;
  border-top: 1px solid #e0e6e8;
  margin: 10px;
  display: flex;
  ${clearFix()} & textarea {
    margin-top: 10px;
  }
`;

const PublishActions = styled.div`
  height: 36px;
  padding: 0 10px;
  ${clearFix()};
  & button {
    float: right;
    width: 120px;
    margin: 0;
    box-shadow: none;
    margin-top: 2px;
    margin-left: 8px;
  }
`;

const ItemDate = styled.div`
  background: transparent;
  border-color: #b7bfc6;
  color: #646f79;
  fill: #848f99;
  align-items: center;
  border: 1px solid transparent;
  border-radius: 18px;
  display: inline-flex;
  flex: 1 1 auto;
  min-width: 1px;
  padding: 3px;
  padding-right: 10px;
  position: relative;
  transition: 0.2s box-shadow, 0.2s color;
  float: left;
  margin-bottom: 0px;
  cursor: pointer;
  &:hover {
    background-color: #f6f8f9;
    border-color: #646f79;
    border-style: solid;
    color: #222b37;
    fill: #222b37;
    cursor: pointer;
    & u {
      background-color: #f6f8f9;
      border-color: #646f79;
      border-style: solid;
      color: #222b37;
      fill: #222b37;
    }
  }
  & u {
    box-sizing: border-box;
    color: #848f99;
    fill: #848f99;
    flex: 0 0 auto;
    font-size: 13px;
    height: 30px;
    line-height: 1;
    transition: 200ms box-shadow, 200ms color, 200ms background, 200ms fill;
    width: 30px;
    background: #fff;
    border: 1px solid #b7bfc6;
    border-radius: 50%;
    align-items: center;
    border-style: dashed;
    display: flex;
    flex-shrink: 0;
    justify-content: center;
    transition: none;
  }

  & > div {
    display: inline-block;
    margin-top: 2px;
  }

  & input {
    border: none;
    font-size: 14px;
    padding: 0;
    height: 36px;
    width: 70px;
    font-weight: 500;
    background: transparent;
    margin: 0;
    margin-left: 10px;
    height: 30px;
    line-height: 30px;
    color: #757575;
    font-size: 13px;
    font-weight: 400;
  }
`;

const StartDate = props => {
  const handleChange = value => {
    props.onChange('date', value);
  };
  const FORMAT = 'D/mmm/YYYY';
  return (
    <ItemDate>
      <u>
        <Calendar width={16} height={16} strokeWidth={1} color="#b7bfc6" />
      </u>
      <DayPickerInput
        formatDate={formatDate}
        parseDate={parseDate}
        format={FORMAT}
        value={`${formatDate(props.value)}`}
        onDayChange={handleChange}
      />
    </ItemDate>
  );
};

export default LogEvent;
