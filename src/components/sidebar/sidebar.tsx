import * as React from 'react';
import styled from '../../themes/styled';
import { NavLink } from 'react-router-dom';
import { graphql, GraphqlQueryControls, OperationOption } from 'react-apollo';
import Relationship from '../../types/Relationship';
import { compose } from 'recompose';
// import Button from '../elements/Button/Button'
import { ellipsis } from 'polished';
import { LoadingMini, ErrorMini } from '../elements/Loading';
const GET_RELATIONSHIPS = require('../../graphql/queries/getMyRelationships.graphql');

interface SidebarProps {
  param: string;
  location: any;
  data: Data;
}

const token = localStorage.getItem('oce_token');
interface Data extends GraphqlQueryControls {
  viewer: {
    myAgent: {
      agentRelationships: Relationship[];
    };
  };
}

const withRelationships = graphql<
  {},
  {
    data: {
      viewer: Data;
    };
  }
>(GET_RELATIONSHIPS, {
  options: (props: SidebarProps) => ({
    variables: {
      token: token
    }
  })
}) as OperationOption<{}, {}>;

const Sidebar: React.SFC<SidebarProps> = props => {
  if (props.data.loading)
    return (
      <Wrapper>
        <LoadingMini />
      </Wrapper>
    );
  if (props.data.error)
    return (
      <Wrapper>
        <ErrorMini
          loading={props.data.loading}
          refetch={props.data.refetch}
          message={`Error! ${props.data.error.message}`}
        />
      </Wrapper>
    );
  let agents = props.data.viewer.myAgent.agentRelationships;
  return (
    <Wrapper>
      <NavList>
        <NavLink
          to={'/'}
          isActive={(match, location) => {
            return location.pathname === `/`;
          }}
          activeStyle={{
            position: 'relative',
            color: '#fff'
          }}
        >
          <Item>
            <span role="img" aria-label="shroom">
              🍄
            </span>{' '}
            Home
          </Item>
        </NavLink>
      </NavList>

      <NavList>
        <Title>Groups</Title>
        {agents.map((a, i) => (
          <React.Fragment key={i}>
            <NavLink
              isActive={(match, location) => {
                return (
                  location.pathname === `/agent/${a.object.id}/` ||
                  location.pathname === `/agent/${a.object.id}`
                );
              }}
              activeStyle={{
                position: 'relative',
                color: '#fff'
              }}
              to={'/agent/' + a.object.id}
            >
              <Item>{a.object.name}</Item>
            </NavLink>
          </React.Fragment>
        ))}
      </NavList>
      {/* <Bottom>
          <Button>Create a new group</Button>
        </Bottom> */}
    </Wrapper>
  );
};

const NavList = styled.div`
  margin-bottom: 24px;
  & a {
    text-decoration: none;
    color: #f0f0f0e6;
    margin-bottom: 16px;
    display: block;

    &: before {
      position: absolute;
      content: '';
      left: -16px;
      top: 0;
      bottom: 0;
      width: 4px;
      display: block;
      background: ${props => props.theme.styles.colour.primary};
      height: 20px;
    }
  }
`;
// const Bottom = styled.div`
// bottom: 8px;
// left: 0;
// right: 0;
// position: absolute;
// & button {
//   width: 100%;
//   display: block;
// }
// `;

// const Title = styled.div`
//   color: #3b99fc;
//   font-weight: 600;
//   font-size: 14px;
//   letter-spacing: 1px;
//   margin-top: 8px;
//   a {
//     text-decoration: none;
//     width: 28px;
//     height: 28px;
//     display: inline-block;
//     color: #3b99fc;
//   }
//   & span {
//     font-size: 16px;
//     padding-left: 5px;
//     margin-right: 8px;
//   }
// `;

// const ListTitle = styled.div`
//   color: #bebebe;
//   font-size: 12px;
//   text-transform: uppercase;
//   margin-bottom: 16px;
//   font-weight: 300;
//   letter-spacing: 1px;
//   margin-top: 32px;
// `;

const Wrapper = styled.div`
  width: 240px;
  display: flex;
  min-width: 240px;
  flex-direction: column;
  padding: 16px;
  position: relative;
  background-color: #151b26;
`;

// const List = styled.div`
// margin-bottom: 24px;
// & a {
//   text-decoration: none;
//   color: ${props => props.theme.styles.colour.base2};
//   margin-bottom: 8px;
//   display: block;

//   &: before {
//     position: absolute;
//     content: '';
//     left: -16px;
//     top: 0;
//     bottom: 0;
//     width: 4px;
//     display: block;
//     background: ${props => props.theme.styles.colour.primary};
//     height: 20px;
//   }
// }
// `;
const Title = styled.div`
  font-size: 12px;
  text-transform: uppercase;
  font-weight: 700;
  margin-bottom: 16px;
  letter-spacing: 0.5px;
  color: #ffffffb3;
`;

const Item = styled.div`
  font-size: 13px;
  font-weight: 400;
  color: inherit;
  letter-spacing: 0px;
  color: inherit;
  ${ellipsis('210px')};
`;

// const Item = styled.h3`
//   margin-top: 8px;
//   letter-spacing: 0.5px;
//   font-weight: 400;
//   font-size: 12px;
//   border-radius: 2px;
//   margin: 0 0 8px 0;
//   height: 30px;
//   cursor: pointer;
//   &:hover {
//     background: rgba(250, 250, 250, 0.2);
//   }
//   & a {
//     text-decoration: none;
//     color: #bebebe;
//     font-size: 13px;
//     text-decoration: none;
//     height: 30px;
//     line-height: 30px;
//     padding: 0 8px;
//     ${ellipsis('210px')};
//     display: block;
//   }
// `;

export default compose(withRelationships)(Sidebar);
