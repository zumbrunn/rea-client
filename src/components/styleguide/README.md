Components within this folder are used only within the styleguide.

- ColourBlock: used to demonstrate the different colours of the theme.

- FontWeight: used to demonstrate the different font styles of the theme.
