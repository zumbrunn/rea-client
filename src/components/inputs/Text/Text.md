```js
<Text placeholder="Input" style={{marginBottom: '10px'}} />

<Text placeholder="Input hovered" hovered={true} style={{marginBottom: '10px'}} />

<Text placeholder="Input focused" focused={true} style={{marginBottom: '10px'}} />

<Text placeholder="Input completed" value="Input completed" style={{marginBottom: '10px'}} />
```
