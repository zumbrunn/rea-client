```js
<Textarea placeholder="Textarea" style={{marginBottom: '10px'}} />

<Textarea placeholder="Textarea hovered" hovered={true} style={{marginBottom: '10px'}} />

<Textarea placeholder="Textarea focused" focused={true} style={{marginBottom: '10px'}} />

<Textarea placeholder="Textarea completed" value="Textarea completed" style={{marginBottom: '10px'}} />
```
