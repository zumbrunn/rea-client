import Agent from './Agent'
import Resource from './Resource'
import Quantity from './Quantity'

export default interface Event {
    id: string;
    action: string;
    start: string;
    validations: {
      id: string;
      validatedBy: Agent
      length: string
    };
    affectedQuantity: Quantity;
    note: string;
    provider: Agent;
    affects: Resource;
    inputOf: {
      id: string;
    }
    outputOf: {
      id: string;
    }
    scope: {
      id: string;
      name: string;
    }
  }
  