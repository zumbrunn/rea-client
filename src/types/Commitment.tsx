import Agent from './Agent';
import Event from './Event';
import Quantity from './Quantity';

interface FulfilledBy {
  fulfilledBy: Event;
}

export default interface Commitment {
  id: string;
  length: number;
  isDeletable: boolean;
  isFinished: boolean;
  userIsAuthorizedToUpdate: boolean;
  userIsAuthorizedToDelete: boolean;
  fulfilledBy: FulfilledBy[];
  provider: Agent;
  inputOf: {
    id: string;
    name: string;
  };
  outputOf: {
    id: string;
    name: string;
  };
  scope: {
    id: string;
    name: string;
  };
  note: string;
  action: string;
  committedQuantity: Quantity;
  due: string;
  resourceClassifiedAs: {
    id: string;
    name: string;
  };
}
