export default interface Resource {
  resourceClassifiedAs: {
    name: string;
    id: string;
  };
  trackingIdentifier: string
}
