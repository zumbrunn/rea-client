import * as React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';

import getApolloClient from './apollo/client';
import registerServiceWorker from './registerServiceWorker';
import App from './containers/App/App';
import { injectGlobal } from './themes/styled';

run();

async function run() {
  const apolloClient = await getApolloClient();

  injectGlobal`
  @import url('https://fonts.googleapis.com/css?family=Fira+Sans:300,400,400i,500,500i,700');
  html {
    font-family: 'Fira Sans', sans-serif !important;
    font-size: 100% !important;
  }
  body {
    padding: 0;
    margin: 0;
    font-family: 'Fira Sans', sans-serif;
    text-rendering: optimizelegibility;
    font-feature-settings: "kern";
    text-size-adjust: none;
    width: 100%;
    height: 100%;
    padding: 0;
    background-color: #f5f6f7;
  }
  ::-webkit-scrollbar {
    width: 8px;
    height: 8px;
  }

  ::-webkit-scrollbar-corner {
      background: transparent;
  }

  ::-webkit-scrollbar-thumb {
      border-radius: 0;
  }

  ::-webkit-scrollbar-thumb {
      background: #313543;
      border: 0 none #fff;
  }

  ::-webkit-scrollbar-track {
      border: 0 none #fff;
      border-radius: 0;
      background: rgba(0,0,0,.1);
  }
 
      * {
        box-sizing: border-box;
      }
  `;

  const ApolloApp = () => (
    <ApolloProvider client={apolloClient}>
      <App />
    </ApolloProvider>
  );

  ReactDOM.render(<ApolloApp />, document.getElementById('root'));

  registerServiceWorker();
}
