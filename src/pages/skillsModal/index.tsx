import * as React from 'react';
import Modal from '../../components/elements/Modal';
import styled from '../../themes/styled';
import H5 from '../../components/typography/H5/H5';
import { compose } from 'react-apollo';
import { LoadingMini, ErrorMini } from "../../components/elements/Loading";
import { graphql, GraphqlQueryControls, OperationOption } from "react-apollo";
import SkillSelect from './skillsSelect'
// import SkillSelect from './skill'
const GET_SKILLS = require('../../graphql/queries/getSkills.graphql');


const token = localStorage.getItem("oce_token");
interface Data extends GraphqlQueryControls {
  client: any;
  viewer: {
    myAgent: any;
  };
}

interface Props {
  toggleModal?: any;
  modalIsOpen?: boolean;
  communityId?: string;
  communityExternalId?: string;
  errors: any;
  touched: any;
  isSubmitting: boolean;
  providerId: string;
  data: Data;
}


const withSkills = graphql<
  {},
  {
    data: {
      viewer: Data;
    };
  }
>(GET_SKILLS, {
  options: (props: Props) => ({
    variables: {
      id: props.providerId,
      token: token
    }
  })
}) as OperationOption<{}, {}>;

const SkillModal:React.SFC<Props> = (props: Props) => {
  const { toggleModal, modalIsOpen, data } = props;
    if (data.loading) return <LoadingMini />;
    if (data.error)
      return (
        <ErrorMini
          loading={data.loading}
          refetch={data.refetch}
          message={`Error! ${data.error.message}`}
        />
      );
    let skills = data.viewer.myAgent.agentSkillRelationships.map(s => ({
      value: s.resourceClassification.id,
      label: s.resourceClassification.name
    }));
  return (
    <Modal isOpen={modalIsOpen} toggleModal={toggleModal}>
      <Container>
        <Header>
          <H5>Edit your skills</H5>
        </Header>
        <Body>
          <SkillsWrapper>
            <SkillSelect providerId={props.providerId} data={data} skills={skills} />
          </SkillsWrapper>
        </Body>
      </Container>
    </Modal>
  );
};

export default compose(withSkills)(SkillModal);

const Body = styled.div`
  
`;

const SkillsWrapper = styled.div`
  
`;

const Container = styled.div`
  font-family: ${props => props.theme.styles.fontFamily};
`;

const Header = styled.div`
  height: 60px;
  border-bottom: 1px solid rgba(151, 151, 151, 0.2);
  & h5 {
    text-align: center !important;
    line-height: 60px !important;
    margin: 0 !important;
  }
`
