import React from 'react';
import { compose } from 'recompose';
import styled from '../../themes/styled';
import { graphql, GraphqlQueryControls, OperationOption } from 'react-apollo';
import { LoadingMini, ErrorMini } from '../../components/elements/Loading';
// Import React Table
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import Resource from '../../types/Resource';
const GET_RESOURCES = require('../../graphql/queries/getInventory.graphql');
const token = localStorage.getItem('oce_token');

interface Data extends GraphqlQueryControls {
  viewer: {
    agent: {
      id: string;
      ownedEconomicResources: Resource[];
    };
  };
}

const withResources = graphql<
  {},
  {
    data: {
      viewer: Data;
    };
  }
>(GET_RESOURCES, {
  options: (props: Props) => {
    return {
      variables: {
        id: props.id,
        token: token
      }
    };
  }
}) as OperationOption<{}, {}>;

const columns = [
  {
    Header: 'Name',
    columns: [
      {
        Header: 'Identifier',
        accessor: 'identifier'
      },
      {
        Header: 'Name',
        id: 'name',
        accessor: d => d.name
      }
    ]
  },
  {
    Header: 'Current Quantity',
    columns: [
      {
        Header: 'Quantity',
        accessor: 'quantity'
      },
      {
        Header: 'Unit',
        accessor: 'unit'
      }
    ]
  },
  {
    Header: 'Category',
    columns: [
      {
        Header: 'Taxonomy',
        accessor: 'taxonomy'
      },
      {
        Header: 'Process Category',
        accessor: 'processCategory'
      }
    ]
  }
];

interface Props {
  data: any;
  id: string;
}

const Home: React.SFC<Props> = props => {
  const { loading, error, refetch, viewer } = props.data;
  if (loading) return <LoadingMini />;
  if (error)
    return (
      <ErrorMini
        loading={loading}
        refetch={refetch}
        message={`Error! ${error.message}`}
      />
    );
  var datat = viewer.agent.ownedEconomicResources.map(r => ({
    identifier: r.trackingIdentifier,
    name: r.resourceClassifiedAs.name,
    quantity: r.currentQuantity.numericValue,
    unit: r.currentQuantity.unit.name,
    taxonomy: r.resourceClassifiedAs.category,
    processCategory: r.resourceClassifiedAs.processCategory
  }));
  return (
    <Container>
      <ReactTable
        data={datat}
        filterable
        // defaultFilterMethod={(filter, row) =>
        //   String(row[filter.id]) === filter.value}
        defaultFilterMethod={(filter, row, column) => {
          const id = filter.pivotId || filter.id;
          if (typeof filter.value === 'object') {
            return row[id] !== undefined
              ? filter.value.indexOf(row[id]) > -1
              : true;
          } else {
            return row[id] !== undefined
              ? String(row[id]).indexOf(filter.value) > -1
              : true;
          }
        }}
        columns={columns}
        defaultPageSize={10}
        className="-striped -highlight"
        style={{ flex: 1 }}
      />
    </Container>
  );
};

const Container = styled.div`
  padding: 10px;
  background: white;
  margin: 10px;
  border-radius: 6px;
  border: 1px solid #dddfe2;
`;

export default compose(withResources)(Home);
