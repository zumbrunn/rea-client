import React from 'react';
import styled from '../../themes/styled';
import { compose, withState, withHandlers } from 'recompose';
// import { PropsRoute } from "../../helpers/router";
const { getUserQuery } = require('../../graphql/getUser.client.graphql');
import { graphql, GraphqlQueryControls, OperationOption } from 'react-apollo';
import { Clock, Users, Left } from '../../components/elements/Icons';
import moment from 'moment';
const token = localStorage.getItem('oce_token');
import { LoadingMini, ErrorMini } from '../../components/elements/Loading';
const GET_PLAN = require('../../graphql/queries/getPlan.graphql');
import { clearFix } from 'polished';
import Overview from '../agent/overview';
import Process from '../../components/elements/Process/Process';
import ProcessModal from '../../components/elements/ModalProcess/ModalProcess';
import { Switch, NavLink } from 'react-router-dom';
import ProtectedRoute from '../../containers/App/ProtectedRoute';

interface Props {
  match: any;
  viewer: any;
  loading: any;
  error: any;
  data: any;
  type: string;
  onType(string): string;
  isOpen: boolean;
  onOpen(boolean): boolean;
  closeModal(string): boolean;
  openModal(string): any;
  location: any;
}

interface Data extends GraphqlQueryControls {
  agent: {
    agentEconomicEvents: Event[];
  };
}

type Response = {
  viewer: Data;
};

const withPlan = graphql<{}, Response>(GET_PLAN, {
  options: (props: Props) => ({
    variables: {
      id: props.match.params.id,
      token: token
    }
  }),
  props: ({ data, ownProps }) => ({
    ...data,
    ...ownProps
  })
}) as OperationOption<{}, {}>;

const Component: React.SFC<Props> = props => {
  let plan;
  let events: any = [];
  let user = props.data.user;
  if (props.loading) {
    return <LoadingMini />;
  } else if (props.error) {
    return (
      <ErrorMini
        loading={props.loading}
        refetch={props.data.refetch}
        message={`Error! ${props.error.message}`}
      />
    );
  }
  plan = props.viewer.plan;
  plan.planProcesses.map(p =>
    p.committedInputs.map(i =>
      i.fulfilledBy.map(f => events.push(f.fulfilledBy))
    )
  );
  return (
    <Wrapper>
      <Container>
        <Header>
          <Scope to={`/agent/${plan.scope[0].id}`}>
            <span>
              <Left width={18} height={18} strokeWidth={2} color={'#3b99fc'} />
            </span>
            {plan.scope[0].name}
          </Scope>
          <Due>
            <span>
              <Clock width={18} height={18} strokeWidth={2} color={'#6f6f6f'} />
            </span>
            {moment(plan.due).format('DD MMM')}
          </Due>
          <Members>
            <span>
              <Users width={18} height={18} strokeWidth={2} color={'#6f6f6f'} />
            </span>
            {plan.workingAgents.slice(0, 6).map((a, i) => {
              return (
                <Img
                  key={i}
                  style={{
                    backgroundImage: `url(${a.image ||
                      `https://www.gravatar.com/avatar/${
                        a.id
                      }?f=y&d=identicon`})`
                  }}
                />
              );
            })}{' '}
            <Tot>
              {plan.workingAgents.length - 6 > 0
                ? `+ ${plan.workingAgents.length - 6} More`
                : ''}
            </Tot>
          </Members>
        </Header>
        <Title>
          <span>plan</span>
          {plan.name}
        </Title>
        <Note>{plan.note}</Note>
        <Menu>
          <MenuItem
            onClick={() => props.onType('overview')}
            active={props.type === 'overview' ? true : false}
          >
            Overview
          </MenuItem>
          <MenuItem
            onClick={() => props.onType('process')}
            active={props.type === 'process' ? true : false}
          >
            Processes
          </MenuItem>
          <MenuItem
            onClick={() => props.onType('conversation')}
            active={props.type === 'conversation' ? true : false}
          >
            Conversations
          </MenuItem>
        </Menu>
      </Container>

      {props.type === 'overview' ? (
        <Overview
          providerId={user.data.id}
          events={plan.planProcesses
            .map(p =>
              p.committedInputs
                .map(i => i.fulfilledBy.map(f => f.fulfilledBy))
                .flat()
            )
            .flat()}
        />
      ) : null}
      {props.type === 'process' ? (
        <Processes>
          {plan.planProcesses.map((process, i) => (
            <Process
              openModal={props.openModal}
              planId={plan.id}
              key={i}
              onOpen={props.onOpen}
              process={process}
            />
          ))}
        </Processes>
      ) : null}
      <Switch>
        <ProtectedRoute
          path={`${props.match.url}/process/:id`}
          component={p => {
            console.log(props);
            return (
              <ProcessModal
                {...p}
                toggleModal={props.onOpen}
                modalIsOpen={props.isOpen}
                closeModal={props.closeModal}
                planId={plan.id}
              />
            );
          }}
        />
      </Switch>
    </Wrapper>
  );
};

const Processes = styled.div`
  margin-bottom: 16px;
  margin: 10px;
  box-sizing: border-box;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-column-gap: 8px;
  grid-row-gap: 8px;
  margin-top: 24px;
`;

const Menu = styled.div`
  background: #f9f9f9;
  height: 40px;
  box-sizing: border-box;
  margin: 0 -16px;
  margin-top: 24px;
`;
const MenuItem = styled.div<{ active?: boolean }>`
  color: ${props => (props.active ? '#3b99fc' : '#848f99')};
  cursor: pointer;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0.5px;
  line-height: 40px;
  display: inline-block;
  padding: 0 8px;
  position: relative;
  text-align: center;
  margin-right: 16px;
  &:hover {
    color: #3b99fc;
  }
  &:before {
    position: absolute;
    content: '';
    bottom: -5px;
    height: 5px;
    border-radius: 8px;
    left: 0;
    width: 100%;
    background-color: ${props => (props.active ? '#3b99fc' : 'transparent')};
  }
  & a {
    color: #bebebe;
    text-decoration: none;
  }
`;

const Header = styled.div`
  ${clearFix()};
  border-bottom: 1px solid #f2f3f2;
  margin: 0 -16px;
  padding-left: 16px;
  height: 40px;
  line-height: 40px;
`;

const Tot = styled.div`
  display: inline-block;
  height: 24px;
  line-height: 24px;
  vertical-align: top;
  margin-left: 4px;
  font-size: 14px;
  color: #cacaca;
  font-weight: 500;
`;

const Members = styled.div`
  font-size: 14px;
  vertical-align: middle;
  height: 40px;
  line-height: 40px;
  float: right;
  & span {
    & svg {
      vertical-align: middle;
    }
    margin-right: 8px;
    margin-left: 8px;
  }
`;

const Img = styled.div`
  width: 24px;
  height: 24px;
  border-radius: 50px;
  display: inline-block;
  margin-left: -4px;
  background-size: cover;
  border: 2px solid white;
  vertical-align: middle;
`;

const Wrapper = styled.div`
  flex: 1;
  margin-bottom: 60px;
  max-width: 1040px;
  width: 100%;
  margin: 0 auto;
  margin-top: 24px;
`;

const Container = styled.div`
  padding: 0 16px;
  padding-bottom: 0;
  background: #fff;
  margin: 10px;
  border-radius: 6px;
  box-sizing: border-box;
  border: 5px solid #e2e5ea;
`;

const Title = styled.h2`
  font-size: 18px;
  margin-top: 4px;
  font-weight: 500;
  color: #44526df7;
  line-height: 32px;
  & span {
    background: #3b99fc;
    border-radius: 3px;
    padding: 0 8px;
    color: #fffffffa;
    font-size: 12px;
    font-weight: 600;
    height: 18px;
    text-transform: uppercase;
    -webkit-letter-spacing: 2px;
    -moz-letter-spacing: 2px;
    -ms-letter-spacing: 2px;
    letter-spacing: 2px;
    line-height: 18px;
    display: inline-block;
    margin-right: 8px;
    vertical-align: middle;
  }
`;

const Note = styled.p`
  font-size: 14px;
  margin-top: 4px;
  color: #484b52d4;
  line-height: 20px;
`;

const Scope = styled(NavLink)`
  font-size: 14px;
  color: #3b99fc;
  display: inline-block;
  vertical-align: middle;
  font-weight: 400;
  height: 30px;
  line-height: 30px;
  margin-right: 16px;
  & span {
    & svg {
      vertical-align: middle;
    }
    margin-right: 8px;
  }
`;

const Due = styled.div`
  font-size: 14px;
  color: #6f6f6f;
  display: inline-block;
  vertical-align: middle;
  font-weight: 400;
  height: 30px;
  line-height: 30px;
  margin-right: 16px;
  & span {
    & svg {
      vertical-align: middle;
    }
    margin-right: 8px;
  }
`;
export default compose(
  withPlan,
  graphql(getUserQuery),
  withState('type', 'onType', 'overview'),
  withState('isOpen', 'onOpen', true),
  withHandlers({
    closeModal: props => val => {
      props.onOpen(false);
      return props.history.push(val);
    },
    openModal: props => val => {
      props.onOpen(true);
      return props.history.push(val);
    }
  })
)(Component);
