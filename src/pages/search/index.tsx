import * as React from 'react';
import { graphql, GraphqlQueryControls, OperationOption } from 'react-apollo';
import { LoadingMini, ErrorMini } from '../../components/elements/Loading';
import Todo from '../Todo/Todo';
const SEARCH_COMMITMENTS = require('../../graphql/queries/searchCommitments.graphql');
import { compose } from 'recompose';

import styled from '../../themes/styled';

const token = localStorage.getItem('oce_token');
interface Data extends GraphqlQueryControls {
  viewer: {
    myAgent: {
      id: string;
      agentRelationships: any[];
    };
  };
}

interface Props {
  providerId: string;
  match: any;
  providerName: string;
  data: Data;
  filter: string;
  id: string;
  providerImage: string;
  toggleValidationModal: any;
  onFilter(string): string;
  page: number;
  onPage(number): number;
}

const withRequirements = graphql<
  {},
  {
    data: {
      viewer: Data;
    };
  }
>(SEARCH_COMMITMENTS, {
  options: (props: Props) => {
    console.log(props);
    return {
      variables: {
        name: props.match.params.id,
        token: token,
        page: props.page
      }
    };
  }
}) as OperationOption<{}, {}>;

const Requirements: React.SFC<Props> = props => {
  const { loading, error, refetch, viewer } = props.data;
  if (loading) return <LoadingMini />;
  if (error)
    return (
      <ErrorMini
        loading={loading}
        refetch={refetch}
        message={`Error! ${error.message}`}
      />
    );
  return (
    <EventsInfo>
      {viewer.myAgent.agentRelationships.map((a, i) => {
        return (
          <div key={a.object.name + i}>
            <Title>{a.object.name}</Title>
            <Todo
              key={i}
              activeIntents={a.object.searchAgentCommitments}
              providerId={props.providerId}
              providerImage={props.providerImage}
              toggleValidationModal={props.toggleValidationModal}
            />
          </div>
        );
      })}
    </EventsInfo>
  );
};

const EventsInfo = styled.div`
  margin-bottom: 16px;
  overflow: overlay;
  max-width: 1040px;
  margin: 0 auto;
  width: 100%;
`;

const Title = styled.div`
  font-size: 32px;
  font-weight: 600;
  padding-bottom: 8px;
  color: #151b26;
  border-bottom: 1px solid;
  margin-top: 24px;
`;

export default compose(withRequirements)(Requirements);
