import React from 'react';
import Feed from '../../components/elements/Event';
import moment from 'moment';
import { clearFix } from 'polished';
import { graphql, GraphqlQueryControls, OperationOption } from 'react-apollo';
import { LoadingMini, ErrorMini } from '../../components/elements/Loading';
import styled from '../../themes/styled';
import Event from '../../types/Event';
import { compose } from 'recompose';
const GET_FEED = require('../../graphql/queries/getFeed.graphql');

const token = localStorage.getItem('oce_token');
interface Data extends GraphqlQueryControls {
  viewer: {
    filteredEconomicEvents: Event[];
  };
}

interface Props {
  providerId: string;
  data: Data;
  id: string;
}

const withFeed = graphql<
  {},
  {
    data: {
      viewer: Data;
    };
  }
>(GET_FEED, {
  options: (props: Props) => ({
    variables: {
      id: props.id,
      token: token
    }
  })
}) as OperationOption<{}, {}>;

const Profile: React.SFC<Props> = props => {
  let comments;
  if (props.data.loading) {
    comments = <LoadingMini />;
  } else if (props.data.error) {
    comments = (
      <ErrorMini
        loading={props.data.loading}
        refetch={props.data.refetch}
        message={`Error! ${props.data.error.message}`}
      />
    );
  } else if (props.data.viewer) {
    comments = (
      <>
        <Events>
          {props.data.viewer.filteredEconomicEvents.map((ev: Event, i) => (
            <Feed
              scopeId={ev.scope.id}
              commitmentId={
                ev.inputOf ? ev.inputOf.id : ev.outputOf ? ev.outputOf.id : ''
              }
              image={
                ev.provider
                  ? ev.provider.image ||
                    `https://www.gravatar.com/avatar/${
                      ev.provider.id
                    }?f=y&d=identicon`
                  : ''
              }
              key={i}
              id={ev.id}
              loggedUserId={props.providerId}
              providerId={ev.provider.id}
              withDelete
              validations={ev.validations}
              primary={
                <FeedItem>
                  <B>{ev.provider ? ev.provider.name : ''}</B>{' '}
                  {ev.action +
                    ' ' +
                    ev.affectedQuantity.numericValue +
                    ' ' +
                    ev.affectedQuantity.unit.name +
                    ' of '}
                  <i>{ev.affects.resourceClassifiedAs.name}</i>
                </FeedItem>
              }
              secondary={ev.note}
              date={moment(ev.start).format('DD MMM')}
            />
          ))}
        </Events>
      </>
    );
  }
  return <Contribution>{comments}</Contribution>;
};

export default compose(withFeed)(Profile);

const Events = styled.div`
  ${clearFix()};
  position: relative;
  padding: 10px;
  background: white;
  border-radius: 3px;
  margin: 10px;
`;
const Contribution = styled.div`
  background: #fff;
  padding: 8px;
  border: 1px solid #e6ecf0;
  background: #fff;
  border: 1px solid #e4e4e4;
  border-radius: 4px;
`;

const FeedItem = styled.div`
  font-size: ${props => props.theme.styles.fontSize.sm};
  color: ${props => props.theme.styles.colour.base1};
`;

const B = styled.b`
  font-weight: 500;
  color: #32211b;
`;
