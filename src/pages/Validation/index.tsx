import React from "react";
import Component from "./validation";
import { compose, withHandlers, withState } from "recompose";
// import ContributionModal from "../components/contributionModal/wrapper";
// import { graphql } from 'react-apollo';
// const { getUserQuery } = require('../../graphql/getUser.client.graphql');
// import Alert from "../alert";
// const Claim = require("../../graphql/queries/getEvent")
// const createValidation = require("../../graphq/mutations/createValidation")
// const deleteValidation = require("../../graphql/mutations/deleteValidation")
import styled from '../../themes/styled';
import Event from '../../types/Event'

import ContributionModal from '../../components/elements/modalValidation'
interface Props {
    id: string
    providerId: string
    toggleModal: any
    modalIsOpen: boolean
    modalId: string
    events: Event[]
    closeModal(): boolean 
}

const CanvasWrapper: React.SFC<Props> = props => {
    const {
      providerId,
      toggleModal,
      events,
      id,
      modalIsOpen,
      modalId,
    } = props;
    return (
      <Container>
        <Validation>
            <Component
              id={id}
              events={events}
              toggleModal={toggleModal}
            />
          </Validation>
          <ContributionModal
            modalIsOpen={modalIsOpen}
            closeModal={toggleModal}
            toggleModal={toggleModal}
            contributionId={modalId}
            myId={providerId}
          /> 
      </Container>
    );
  }

const Container = styled.div``
const Validation = styled.div``


export default compose(
  withState("modalIsOpen", "toggleModalIsOpen", false),
  withState("modalId", "selectModalId", null),
  withHandlers({
    toggleModal: props => contributionId => {
      props.selectModalId(contributionId);
      props.toggleModalIsOpen(!props.modalIsOpen);
      return null
    },
    closeModal: props => () => {
      return props.toggleModalIsOpen(false);
    }
  })
)(CanvasWrapper);
