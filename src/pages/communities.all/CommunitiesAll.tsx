import * as React from 'react';
import { compose } from 'recompose';
import { graphql, GraphqlQueryControls, OperationOption } from 'react-apollo';
import styled from '../../themes/styled';

import { Trans } from '@lingui/macro';
import Relationship from '../../types/Relationship';

// import H4 from '../../components/typography/H4/H4';
import Main from '../../components/chrome/Main/Main';
import Loader from '../../components/elements/Loader/Loader';
import CommunityCard from '../../components/elements/Community/Community';
import media from 'styled-media-query';
// import CommunitiesLoadMore from '../../components/elements/Loadmore/community';
import { Community, Eye } from '../../components/elements/Icons';
import { SuperTab, SuperTabList } from '../../components/elements/SuperTab';
import { Tabs, TabPanel } from 'react-tabs';
// import CommunitiesJoined from '../communities.joined';
// const { getCommunitiesQuery } = require('../../graphql/getCommunities.graphql');
const GET_RELATIONSHIPS = require('../../graphql/queries/getMyRelationships.graphql');
const token = localStorage.getItem('oce_token');
interface Data extends GraphqlQueryControls {
  viewer: {
    myAgent: {
      agentRelationships: Relationship[];
    };
  };
}
interface SidebarProps {
  param: string;
  location: any;
  data: Data;
}

const withRelationships = graphql<
  {},
  {
    data: {
      viewer: Data;
    };
  }
>(GET_RELATIONSHIPS, {
  options: (props: SidebarProps) => ({
    variables: {
      token: token
    }
  })
}) as OperationOption<{}, {}>;


class CommunitiesYours extends React.Component<SidebarProps> {
  render() {
    return (
      <Main>
        <WrapperCont>
          <Wrapper>
            <Tabs>
              <SuperTabList>
                <SuperTab>
                  <span>
                    <Community
                      width={20}
                      height={20}
                      strokeWidth={2}
                      color={'#a0a2a5'}
                    />
                  </span>
                  <h5>
                    <Trans>All communities</Trans>
                  </h5>
                </SuperTab>
                <SuperTab>
                  <span>
                    <Eye
                      width={20}
                      height={20}
                      strokeWidth={2}
                      color={'#a0a2a5'}
                    />
                  </span>
                  <h5>
                    <Trans>Joined communities</Trans>
                  </h5>
                </SuperTab>
              </SuperTabList>
              <TabPanel>
                {this.props.data.error ? (
                  <span>
                    <Trans>Error loading communities</Trans>
                  </span>
                ) : this.props.data.loading ? (
                  <Loader />
                ) : (
                  <>
                    <List>
                      {this.props.data.viewer.myAgent.agentRelationships.map((community, i) => {
                        return (
                          <CommunityCard
                            key={i}
                            summary={community.object.note}
                            title={community.object.name}
                            icon={community.object.image || ''}
                            id={community.object.id}
                            followed={true}
                            followersCount={1}
                            collectionsCount={1}
                            externalId={'community.id'}
                            threadsCount={1}
                          />
                        );
                      })}
                    </List>
                    {/* <CommunitiesLoadMore
                      fetchMore={this.props.data.fetchMore}
                      communities={this.props.data.communities}
                    /> */}
                  </>
                )}
              </TabPanel>
              <TabPanel>
                {/* <CommunitiesJoined /> */}
                ciao
              </TabPanel>
            </Tabs>
          </Wrapper>
        </WrapperCont>
      </Main>
    );
  }
}

export const WrapperCont = styled.div`
  max-width: 1040px;
  margin: 0 auto;
  width: 100%;
  height: 100%;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  margin-bottom: 24px;
  background: white;
  border-radius: 6px;
  box-shadow: 0 1px 7px 0px rgba(0, 0, 0, 0.1);
  margin-top: 16px;
  & ul {
    display: block !important;

    & li {
      display: inline-block;

      & h5 {
        font-size: 13px;
        font-weight: 500;
        color: ${props => props.theme.styles.colour.base3};
      }
    }
  }
  & h4 {
    margin: 0;
    font-weight: 400 !important;
    font-size: 14px !important;
    color: #151b26;
    line-height: 40px;
  }
`;
export const List = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-column-gap: 16px;
  grid-row-gap: 16px;
  padding-top: 0;
  padding: 16px;
  ${media.lessThan('medium')`
  grid-template-columns: 1fr;
  `};
`;

export default  compose(withRelationships)(CommunitiesYours);
